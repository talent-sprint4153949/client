import React, {  useRef ,useState } from 'react';
import {  Button, Modal, ModalHeader, ModalBody, ModalFooter,  Form, FormGroup,  } from 'reactstrap';
import axios from 'axios'

export default function ModelCheckOut({modal, setModal, productName, productId}) {
    const nameRef = useRef();
    const mailRef = useRef();
    const numberRef = useRef();
    const quantityRef = useRef();
    const [validationErrors, setValidationErrors] = useState({});

    const validateEmail = (email) =>{
        const emailRegex = /^\S+@\S+\.\S+$/;
        return emailRegex.test(email);

    }
    const validatePhoneNumber = (phoneNumber) => {
        const phoneNumberRegex = /^\d{10}$/;
        return phoneNumberRegex.test(phoneNumber);
      };
  
    const handelsubmit = () => {
      let isValid = true;
      const newValidationErrors = {};
  
      // Validate form fields
      if (!nameRef.current.value.trim()) {
        isValid = false;
        newValidationErrors.name = 'Name is required';
      }
  
      if (!mailRef.current.value.trim()) {
        isValid = false;
        newValidationErrors.email = 'Email is required';
      }else if(!validateEmail(mailRef.current.value)){
        isValid = false;
        newValidationErrors.email = 'invalid email format'
      }
  
      if (!quantityRef.current.value.trim()) {
        isValid = false;
        newValidationErrors.count = 'Number of products is required';
      }else if (!validatePhoneNumber(numberRef.current.value)){
        isValid = false;
        newValidationErrors.number ='invalid phone number format';
      }
  
      if (!numberRef.current.value.trim()) {
        isValid = false;
        newValidationErrors.number = 'Number is required';
      }
  
      if (!isValid) {
        setValidationErrors(newValidationErrors);
        alert('Please fill in all the required details'); // You can customize this message or use another UI component
        return;
      }
  
      // If validation passes, proceed with the submission
      const newOrder = {
        productId: parseInt(productId),
        count: parseInt(quantityRef.current.value),
        name: nameRef.current.value,
        number: parseInt(numberRef.current.value),
        email: mailRef.current.value,
      };
  
      axios
        .post('https://server-aj0x.onrender.com/orders', newOrder)
        .then((res) => {
          console.log('Successful insertion', newOrder);
          setModal(!modal);
        })
        .catch((err) => {
          console.error(err);
        });
  
      alert('Your order is confirmed');
    };
  return (
    <div>
        <Modal isOpen={modal} toggle={()=>setModal(!modal)}>
       <ModalHeader toggle={()=>setModal(!modal)} style={{ background: '#2c2c2c', color: 'white' }}>{productName} </ModalHeader>
        <ModalBody style={{ background: '#2c2c2c', color: 'white' }}>
          <Form>
        
            <FormGroup>
              <input
              className={`form-control ${validationErrors.name ? 'is-invalid':""}`}
                style={{ border: 'none' }}
                type="text"
                name='name'
                id='name'
                required
                placeholder='Enter your name' 
                ref={nameRef}
              />
              {validationErrors.name && <div className='invalid-feedback'>{validationErrors.name}</div>}
            </FormGroup>
            <FormGroup>
              <input
              className={`form-control ${validationErrors.email ? 'is-invalid': ""}`}
                style={{ border: 'none' }}
                type="email"
                name="email"
                id="email"
                required
                placeholder="Enter your email"
                
                ref={mailRef}
              />
              {validationErrors.email &&<div className='invalid-feedback'>{validationErrors.email}</div>}
            </FormGroup>
            <FormGroup>
              <input
              className={`form-control ${validationErrors.count ? 'is-invalid': ""}`}
                 style={{border:'none'}}
                 type="number"
                 name="count"
                 id="count"
                 required
                 placeholder="Number of products "
                 
                 ref={quantityRef}
               />
                {validationErrors.count &&<div className='invalid-feedback'>{validationErrors.count}</div>}
               </FormGroup>
               <FormGroup>
              <input
              className={`form-control ${validationErrors.number? 'is-invalid': ""}`}
                 style={{border:'none'}}
                 type="number"
                 name="number"
                 id="number"
                 required
                 placeholder="Enter your number"
                 
                 ref={numberRef}
               />
                {validationErrors.number &&<div className='invalid-feedback'>{validationErrors.number}</div>}
              </FormGroup>
                  
          </Form>
        </ModalBody>
        <ModalFooter style={{ background: '#2c2c2c' }}>
          <Button color="success" onClick={()=>{handelsubmit(); setModal()}}>
            Book Now
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  )
}