import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Button } from 'reactstrap';

const ShortInfo = () => {

  return (
    <div style={{color: 'white',fontFamily: "'Roboto Condensed', sans-serif",display: "flex",justifyContent: "center", alignItems: "center", width:"100%"}}>
      <Container className="p-4 ">
        <Row className="row-cols-1 row-cols-lg-2 gap-2">
          <Col>
            <h1 style={{ color: '#d4d4d4' }} className="text-3xl font-bold">
              PROFESSIONAL BARBERSHOP <br /> FOR MEN ONLY
            </h1>
          </Col>
          <Col>
            <p className="text-slate-300 text-sm">
            Step into our modern and stylish space, designed exclusively for the modern man. The ambiance combines contemporary aesthetics with classic charm, creating an environment that is both welcoming and sophisticated. The interior features sleek barber chairs, clean lines, and a palette of masculine tones to ensure a comfortable and visually pleasing experience.
            </p>
            <Row className="my-3.5 row-cols-1 row-cols-lg-2 gap-2">
              <Col className="my-3.5">
                <h1 className="text-xl font-bold"> SINCE 2015</h1>
                
                <Button color="danger">
                  <Link to="/about" style={{ color: 'white',  textDecoration: 'none' }}>
                    Learn More
                  </Link>
                </Button>
              </Col>
              <Col className="my-3.5">
                <h1 className="text-xl font-bold">1000+ CLIENTS</h1>
                <p className=" text-slate-300 text-sm">
                Join the ranks of our ever-growing community of over 1000 customers who have chosen us as their go-to destination for top-notch men's grooming. As we look towards the future, we remain dedicated to exceeding expectations, one satisfied customer at a time. 
                </p>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default ShortInfo;
