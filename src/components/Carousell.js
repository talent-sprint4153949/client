

import { useState } from 'react';
import Carousel from 'react-bootstrap/Carousel';


function Carousell() {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex) => {
    setIndex(selectedIndex);
  };

  return (
    <Carousel activeIndex={index} onSelect={handleSelect}>
      <Carousel.Item>
        <img  className="w-100"src={require('../Images/Fashion.png')}text="First slide" />
        <Carousel.Caption>
          
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img  className='w-100' src={require('../Images/perfect.png')}text="Second slide" />
        <Carousel.Caption>
          
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img  className='w-100' src={require('../Images/royal.png')}text="Third slide" />
        <Carousel.Caption>
          
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

export default Carousell;