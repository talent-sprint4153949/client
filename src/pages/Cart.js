import React,{useEffect,useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Card, CardImg, CardBody, CardTitle, CardText, Button, Col, Row } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { remove,  } from '../store/CartSlice';
import Footer from '../components/Footer';

import axios from 'axios';
import ModelCheckOut from '../components/ModelCheckOut';

export default function Cart() {
  const products = useSelector((state) => state.cart);
  const [data, setData] = useState([]);
  const dispatch = useDispatch();
  const [productId, setProductId]=useState(0);
  const [modal, setModal] = useState(false);
  const [productName, setProductName] = useState('');


  const removeTocart = (productId) => {
    dispatch(remove(productId));
  };

  const submit = (productId) => {
    
    removeTocart(productId); 
  };



  useEffect(() => {
    axios.get('https://server-aj0x.onrender.com/products')
      .then((res) => setData(res.data))
      .catch((err) => console.error(err));
  }, []);


  return (
    <div>
      <div style={{ backgroundColor: '#262424', minHeight: '100vh', padding: '20px' }}>
        <p style={{ fontWeight: 'bold' }} className="text-center text-2xl font-bold text-danger">
          Cart
        </p>
        <Row style={{ marginLeft: '100px' }} className="c_padding mt-2">
          {products.map((product) => (
            <Col md={4} className="mb-4" key={product.id}>
              <Card className="bg-neutral shadow-xl" style={{ background: '#2c2c2c', width: '300px' }}>
                <CardImg
                  top
                  src={product.img}
                  className="rounded-xl"
                  style={{ maxWidth: '100%', maxHeight: '330px', objectFit: 'cover' }}
                />
                <CardBody className="items-center text-center" style={{ color: 'white' }}>
                  <CardTitle style={{ fontWeight: 'bold' }}>{product.title}</CardTitle>
                  <CardText>{product.Price}</CardText>

                 

                  <div className="card-actions" style={{ marginTop: '10px' }}>
                    <Button onClick={() => removeTocart(product.id)} color="danger">
                      REMOVE
                    </Button>
                    {' '}
                    <Button onClick={() =>{submit(product.id); setModal(!modal); setProductId(product.id); setProductName(product.title)}} color="success">
                      CHECK-OUT
                    </Button>
                  </div>
                </CardBody>
              </Card>
            </Col>
          ))}
        </Row>
      </div>
      <ModelCheckOut modal={modal} setModal={setModal} productId={productId} productName={productName} />
      <div>
        <Footer />
      </div>
    </div>
  );
}