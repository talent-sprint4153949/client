import React, { useState } from 'react';
import { Card, CardImg, CardBody, CardTitle, CardText, Button, Modal, ModalHeader, ModalBody, ModalFooter, Col, Row, Form, FormGroup, Input } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
// import './Services.css';
import axios from 'axios'
import Footer from '../components/Footer';
// import Footer from '../components/Footer';
const Services = () => {
  return (
    <div>
    <div style={{ backgroundColor: '#262424', minHeight: '100vh', padding: '20px' }}>
      <p style={{ fontWeight: 'bold',fontSize:"35px"  }} className="text-center text-2xl font-bold text-danger">
        Available Services
      </p>
      <Row style={{ marginLeft: '100px' }} className="c_padding mt-3">
         <ServiceCard title="HAIRCUT" imgSrc="https://i.ibb.co/5Rsj4fX/HAIRCUT.jpg" spaces="16" price="₹229" defaultTitle="HAIRCUT" />
        <ServiceCard title="MUSTACHE" imgSrc="https://i.ibb.co/PTbTVs3/MUSTACHE.jpg" spaces="16" price="₹99" />
        <ServiceCard title="SHAVE" imgSrc="https://i.ibb.co/fVWhFGy/SHAVE.jpg" spaces="16" price="₹199" />
        <ServiceCard title="Manicure & Pedicure" imgSrc="https://i.ibb.co/JKQFf3H/Manicure-Pedicure.jpg" spaces="16" price="₹339" />
        <ServiceCard title="BREADTRIM" imgSrc="https://i.ibb.co/R3CJ1qB/BREADTRIM.jpg" spaces="16" price="₹119" />
        <ServiceCard title="HAIR DYEING" imgSrc="https://i.ibb.co/mcpG5CB/HAIR-DYEING.png" spaces="16" price="₹499" />
        <ServiceCard title="FACE PACK" imgSrc="https://img.mensxp.com/media/content/2020/Sep/AMP_img_5f65b9d7446b5.jpeg" spaces="16" price="₹499" />
        <ServiceCard title="CHARCOAL FACE PACK" imgSrc="https://www.shutterstock.com/image-photo/handsome-man-black-charcoal-face-600nw-1679381284.jpg" spaces="16" price="₹299" />     
 </Row>

    </div>
    <Footer />
    </div>
  );
};


const ServiceCard = ({ title, imgSrc, spaces, price,defaultTitle }) => {
  const [showModal, setShowModal] = useState(false);
  const [formData, setFormData] = useState({
    title:"",
    name: "",
    date: "",
    time: "",
    address: "",
    email: "",
    number: "",
  });

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleBuyNow = () => {
    setShowModal(true);
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
   let errorMessage = '';

   const validationFunctions = {
    title: (value) => (!value.trim()? 'Title is required' : ''),
    date: (value) => (!value ? 'Date is required' : ''),
    time: (value) => (!value ? 'Time is required' : ''),
    name: (value) => (!value ? 'Name is required' : ''),
    email: (value) =>
      !value
        ? 'Email is required'
        : !/^\S+@\S+\.\S+$/.test(value)
        ? 'Invalid email format'
        : '',
    address: (value) => (!value.trim() ? 'Address is required' : ''),
    number: (value) =>
      !value
        ? 'Number is required'
        : !/^\d{10}$/.test(value)
        ? 'Invalid phone number format'
        : '',
  };
  errorMessage = validationFunctions[name] ? validationFunctions[name](value):'';
  if (!errorMessage){
    setFormData((prevData)=>({...prevData,[name]:value}));

  }else{
    console.error(`validation Error(${name}:${errorMessage})`)
  }
  };

  const handelsubmit = () => {
    const isAnyfieldEmpty = Object.values(formData).some(value => value ==='');
    if(isAnyfieldEmpty){
      alert('please fill in all the required fields')
      return ;
    }
    const newAppointment ={...formData}


    axios.post('https://server-aj0x.onrender.com/services', newAppointment)
      .then((res) => {
        console.log('Successful insertion', newAppointment);
        handleCloseModal(); // Close the modal after successful submission
      })
      .catch((err) => {
        console.error(err);
      });
      alert("your Appointment in confirmed")
  };

  return (
  
    <Col md={4}  className="mb-4" style={{justifyContent:"center"}}>
        <Card className="bg-neutral shadow-xl" style={{ background: '#2c2c2c', width: '300px',backdropFilter:'blur(20px)' }}>
       <CardImg top src={imgSrc} className="rounded-xl" style={{ width: '100%', height: '200px', objectFit: 'cover' }} />
        <CardBody className="items-center text-center" style={{ color: 'white',backdropFilter:'blur(20px)' }}>
          <CardTitle style={{fontWeight:'bold'}}>{title}</CardTitle>
         
          <CardText>{price}</CardText>
          <div className="card-actions">
            <Button color="danger" onClick={handleBuyNow}>
              Book Now
            </Button>
          </div>
        </CardBody>
      </Card>
      
      <Modal isOpen={showModal} toggle={handleCloseModal}>
       <ModalHeader toggle={handleCloseModal} style={{ background: '#2c2c2c', color: 'white' }}>{title} </ModalHeader>
        <ModalBody style={{ background: '#2c2c2c', color: 'white' }}>
          <Form>
          <FormGroup>
              <Input
                style={{ border: 'none' }}
                type="text"
                name='title'
                id='serviceName'
                placeholder='Enter the name of the Service'
                autoFocus
                required
                value={title}
                onChange={handleInputChange}
              />
            </FormGroup>
            <FormGroup>
              <Input
                style={{ border: 'none' }}
                type="date"
                name='date'
                id='date'
                placeholder='Date of Appointment'
                value={formData.date}
                required
                onChange={handleInputChange}
              />
            </FormGroup>
            <FormGroup>
              <Input
                style={{ border: 'none' }}
                type="time"
                name="time"
                id="time"
                placeholder="Time"
                required
                autoFocus
                value={formData.time}
                onChange={handleInputChange}
              />
            </FormGroup>
            <FormGroup>
              <Input
                 style={{border:'none'}}
                 type="text"
                 name="name"
                 id="name"
                 placeholder="Name"
                 autoFocus
                 required
                 value={formData.name}
                 onChange={handleInputChange}
               />
               </FormGroup>
               <FormGroup>
              <Input
                 style={{border:'none'}}
                 type="email"
                 name="email"
                 id="email"
                 placeholder="Enter Your Email"
                 autoFocus
                 required
                 value={formData.email}
                 onChange={handleInputChange}
               />
              </FormGroup>
              <FormGroup>
              <Input
                 style={{border:'none'}}
                 type="text"
                 name="address"
                 id="address"
                 placeholder="Enter Your Address"
                 autoFocus
                 required
                 value={formData.address}
                 onChange={handleInputChange}
               />
              </FormGroup>
               <FormGroup>
              <Input
                 style={{border:'none'}}
                 type="text"
                 name="number"
                 id="number"
                 placeholder="Enter Your Mobile Number"
                 autoFocus
                 required
                 value={formData.number}
                 onChange={handleInputChange}
               />
              </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter style={{ background: '#2c2c2c' }}>
          <Button color="success" onClick={handelsubmit}>
            Book Now
          </Button>
        </ModalFooter>
      </Modal>
    </Col>
    
    
    
   
  );
  
};

export default Services;
