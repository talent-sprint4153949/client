import React, { useState,useEffect } from 'react';
import {  Form, FormGroup, Label, Input, Button } from 'reactstrap';
import { Link,useNavigate } from 'react-router-dom';
import axios from 'axios';
import './Register.css'
import Footer from '../components/Footer';




const Register = () => {  
  const [data, setData] = useState({ Name: '', Email: '', Password: '' });
  const navigate = useNavigate();
 // password Validation
  const [PasswordError,SetPasswordError] = useState('');
  const ValidatePassword = (Password) =>{
    const PasswordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    if(!PasswordRegex.test(Password)){
      SetPasswordError(
        <h6 style={{fontSize:"10px"}}>password should have special symbol lower and upper case</h6>
      );
      return false;
    }

    SetPasswordError('');
    return true;
    
  }
  // Email verification
 
  

  /// for JSW token and user registered
  useEffect(() => {
    const auth = localStorage.getItem('token');
    if (auth) {
      navigate('/');
    }
  }, [navigate]);
  
 /// for ONCLICK Function and checking the data provided
 const registeredUser = async (e) => {
  e.preventDefault();
  const errors = {};

  if (!data.Email) {
    errors.email = 'Email is required';
    alert('Email is required');
    return;
  }

  if (!data.Password) {
    errors.password = 'Password is required';
    alert('Password is required');
    return;
  } else if (!ValidatePassword(data.Password)) {
    alert('Please enter a valid Password');
    return;
  }

  

  try {
    const registerResponse = await axios.post('https://server-aj0x.onrender.com/register', data);
    if (registerResponse.status === 200) {
      localStorage.setItem('token', 'loggedin');
      window.location.href = '/';
    } else {
      alert('Registration Failed, please try again');
    }
  } catch (error) {
    console.error('Error registering user: ', error); 
    alert('Registration Failed, please try again');
  }
};
    


    // gettng response from the back end server to post the operation



  return (
    <div >
    <div  className='register'>
    <div className="reg-cont" style={{backgroundColor:"#2c2c2c" ,padding:"2% 3%" ,justifyContent:"center"}} >
        <h5 className="text-center mb-2 mt-2" style={{ color: 'white', fontWeight: 'bold', alignItems: 'center' }}>Register</h5>
        <Form onSubmit={registeredUser} style={{ display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
          <FormGroup className="mb-3">
            <Label for="name" style={{ color: 'white', padding: '2px 4px' }}>Name</Label>
            <Input
              type="text"
              placeholder="Enter your name"
              value={data.Name}
              onChange={(e) => setData({ ...data, Name: e.target.value })}
              className="form-control rounded-2  " style={{width:"270px"}}
             
            />
          </FormGroup>
          <FormGroup className="mb-3">
            <Label for="email" style={{ color: 'white', padding: '2px 4px' }}>Email</Label>
            <Input
              type="email"
              placeholder="Enter your email"
              value={data.Email}
              onChange={(e) => setData({ ...data, Email: e.target.value })}
              className="form-control rounded-2 " style={{width:"270px"}}
            />
          </FormGroup>
          <FormGroup className="mb-3">
            <Label for="password" style={{ color: 'white', padding: '2px 4px' }}>Password</Label>
            <Input
              type="password"
              placeholder="Enter your password"
              value={data.Password}
              onChange={(e) => { setData({ ...data, Password: e.target.value })
             ValidatePassword(e.target.value);
            }}
              className="form-control rounded-2"
              style={{width:"270px"}}
            
            />
            {PasswordError && <div className='text-danger'>{PasswordError}</div>}
          </FormGroup>
          <Button type="submit" style={{backgroundColor:'#FF0000' ,color:"#fff" } } className="w-100 rounded-0 mx-auto 16px 0px 0px   ">
            Register
          </Button>
          < p className='text-light mt-2'>
          Already have an account? <Link to="/login" style={{ color: '#FF0000'}} >Login</Link>
        </p>
        </Form>
     </div>
    </div>
    <Footer />
    </div>
  );
};

export default Register;




