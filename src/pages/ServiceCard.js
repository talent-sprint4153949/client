import React from 'react';
import { Card, CardImg, CardBody, CardTitle, CardText, Button } from 'reactstrap';

const ServiceCard = ({ title, imgSrc, description, price, handleBuyNow }) => {
  const handleAddToCart = () => {
    handleBuyNow(title, price, imgSrc);
  };

  return (
    <Card>
      <CardImg top width="100%" src={imgSrc} alt={title} />
      <CardBody>
        <CardTitle>{title}</CardTitle>
        <CardText>{description}</CardText>
        <CardText>Price: {price}</CardText>
        <Button onClick={handleAddToCart}>Add to Cart</Button>
      </CardBody>
    </Card>
  );
};

export default ServiceCard;
