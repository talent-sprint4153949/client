import React, { useState,useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import {  FormGroup } from 'reactstrap';
import axios from 'axios'; // Import axios for making HTTP requests
import './Login.css'
import Footer from '../components/Footer'

const Login = () => {

  const [data, setData] = useState({ Email: '', Password: '' });
  const [errors, setErrors] = useState({ email: '', password: '' });
  const navigate = useNavigate
  useEffect(() => {
    const auth = localStorage.getItem('token');
    if (auth) {
      navigate('/');
    }
  }, [navigate]);

  const validateLogin = () => {
    const errors = {};
    if (!data.Email) {
      errors.email = 'Email is required';
    }
    if (!data.Password) {
      errors.password = 'Password is required';
    }
    setErrors(errors);
    return Object.keys(errors).length === 0;
  };

  const loginUser = async (e) => {
    e.preventDefault();
    if (validateLogin()) {
      try {
        const response = await axios.post('https://server-aj0x.onrender.com/display', data); // Update the URL to your login endpoint
        if (response.status === 200) {
          localStorage.setItem('token', 'loggedin');
          // Redirect to the homepage or any other route upon successful login
          window.location.href = '/';
        } else {
          alert('Login Failed, please try again');
        }
      } catch (error) {
        console.error('Error submitting form', error);
        alert('Login Failed, please try again');
      }
    } else {
      alert('Login failed. Please check the form for errors.');
    }
  };



  return (
    <div >

    <div className='register '>
  <div className="logincont" style={{backgroundColor:"#2c2c2c" ,padding:"2% 2%"}} >  
  <h5 className="text-center mb-2 mt-2" style={{ color: 'white', fontWeight: 'bold' }}>Login</h5>
  <form onSubmit={loginUser}>
    <FormGroup className='mb-3'>
     
        <label htmlFor="email" style={{ color: 'white', padding: '2px 4px' }}>Email</label>
        <input
          type="email"
          placeholder="Enter your email"
          value={data.Email}
          onChange={(e) => setData({ ...data, Email: e.target.value })}
          className="form-control rounded-2"
          
        />
        {errors.email && <div style={{ color: 'red' }}>{errors.email}</div>}
    </FormGroup>
    <FormGroup className='mb-3'>
      <div className="mb-3">
        <label htmlFor="password" style={{ color: 'white', padding: '2px 4px' }}>Password</label>
        <input
          type="password"
          placeholder="Enter your password"
          value={data.Password}
          onChange={(e) => setData({ ...data, Password: e.target.value })}
          className="form-control rounded-2"
          
        />
        {errors.password && <div style={{ color: 'red' }}>{errors.password}</div>}
      </div>
    </FormGroup>
    <button type="submit" style={{ backgroundColor: '#FF0000', color: '#fff'}} className=" w-100 rounded-0 mx-auto 16px 0px 0px btn ">
      Login
    </button>
    <p className="text-light mt-2">
      Don't have an account? <Link to="/register" style={{ color: '#FF0000' }}>Register here.</Link>
    </p>
  </form>
</div>

    </div>
    <Footer />
    </div>
    

  );
};

export default Login;


