import React, { useEffect, useState } from 'react';
import { Card, CardImg, CardBody, CardTitle, CardText, Button, Col, Row } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import { useDispatch } from 'react-redux';
import {add} from '../store/CartSlice';
import Footer from '../components/Footer';
const Shop = () => {
  const dispatch=useDispatch();
  const [products, setProducts] = useState([]);

  useEffect(() => {
    axios.get('https://server-aj0x.onrender.com/products')
      .then((res) => setProducts(res.data))
      .catch((err) => console.error(err));
  }, []);
const addTocart=(product)=>{
   dispatch(add(product))
   alert("Item added to cart")
}


 

  return (
    <div>
    <div style={{ backgroundColor: '#262424', minHeight: '100vh', padding: '20px' }}>
      <p style={{ fontWeight: 'bold' ,fontSize:"35px" }} className="text-center text-2xl font-bold text-danger">
        Products
      </p>
      <Row style={{ marginLeft: '100px' }} className="c_padding mt-2">
        {products.map((product) => (
          <Col md={4} className="mb-4">
          <Card className="bg-neutral shadow-xl" style={{ background: '#2c2c2c', width: '300px' }}>
            <CardImg top src={product.img} className="rounded-xl" style={{ maxWidth: '100%', maxHeight: '330px', objectFit: 'cover' }} />
            <CardBody className="items-center text-center" style={{ color: 'white' }}>
              <CardTitle style={{ fontWeight: 'bold' }}>{product.title}</CardTitle>
              
              <CardText>{product.Price}</CardText>
              <div className="card-actions">
                <Button color="danger" onClick={()=>addTocart(product)}>
                  ADD 
                </Button>
              </div>
            </CardBody>
          </Card>
        </Col>
         
        ))}
      </Row>
    </div>
    <Footer />
    </div>
  );
};



export default Shop;