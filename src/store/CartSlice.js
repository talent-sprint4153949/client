import {createSlice} from '@reduxjs/toolkit'; 


const initialState=[];
const CartSlice=createSlice({
    name:'cart',
    initialState,
    reducers:{
        add(state,action){
            state.push(action.payload)
        },
        remove(state,action){
           return state.filter(item => item.id !== action.payload)

        },
        updateQuantity(state, action) {
            const { productId, quantity } = action.payload;
            const product = state.find((p) => p.id === productId);
            if (product) {
              product.quantity += quantity;
            }
          }
    }
});

export const {add,remove,updateQuantity }=CartSlice.actions; 
export default CartSlice.reducer;